package com.esb.camel.test.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import com.esb.camel.test.model.TestModel;

/**
 * 
 * @author Enrique
 *
 */
@Component(value = "testProcessor")
public class TestProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception{
		
		String payload = exchange.getIn().getBody(String.class);
		
		List<TestModel> list= new ArrayList<>();
		TestModel model=null;
		String array[];
		
		for(String row:payload.trim().split("\\n")){
			
			array=row.trim().split("\\|");
			
			model= new TestModel(null,
			Integer.parseInt(array[0].substring(0, 8)),
				 new BigDecimal(array[1]),
			  Integer.parseInt(array[2]),
			   array[3], 
				 array[4]);
			
			list.add(model);
		}
	
		
		exchange.getOut().setBody(list);
		
		
	}
	
}
