package com.esb.camel.test.model;

import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * 
 * @author Enrique
 *
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5083823927403310411L;

	private Integer id;
	private Integer fechaVenta;
	private BigDecimal monto;
	private Integer sucursal;
	private String cliente;
	private String canal;

	public TestModel(Integer id,Integer fechaVenta,BigDecimal monto,Integer sucursal,String cliente,String canal) {
		this.id=id;
		this.fechaVenta=fechaVenta;
		this.monto=monto;
		this.sucursal=sucursal;
		this.cliente=cliente;
		this.canal=canal;
		
	}
	
	public TestModel() {
		
	}
}
